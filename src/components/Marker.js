import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Tooltip from "@material-ui/core/Tooltip";

const Marker = props => {
  const { contact } = props;
  return (
    <div>
      <Tooltip title={`${contact.Firstname} ${contact.Surname}`}>
        <Avatar>{`${contact.Firstname[0]}${contact.Surname[0]}`}</Avatar>
      </Tooltip>
    </div>
  );
};

export default Marker;