import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";

import Marker from "./components/Marker";
import styles from "./styles/Styles";
import contactsData from "./constants/contactsData.json";

class App extends Component {
  state = {
    distance: 10,
    lat: -33.923536,
    lng: 18.424399,
    contacts: null,
    maps: []
  };

  componentWillMount() {
    this.setState(state => {
      return {
        contacts: contactsData.filter(contact => {
          return (
            state.distance >=
            Math.sqrt(
              Math.pow(contact.Long - state.lng, 2) +
                Math.pow(contact.Lat - state.lat, 2)
            ) *
              100
          );
        })
      };
    });
  }

  searchHandler = event => {
    let { value } = event.target;
    value = value.toLowerCase();

    // Simple fuzzy seatch on name and surname
    // Search either for name/surname, or distannce in km
    
    if (Number.isInteger(Number(value)) && value !== "") {
      this.setState(prevState => {
        return {
          contacts: contactsData.filter(contact => {
            return (
              Number(value) >=
              Math.sqrt(
                Math.pow(contact.Long - prevState.lng, 2) +
                  Math.pow(contact.Lat - prevState.lat, 2)
              ) *
                100
            );
          }),
          distance: value
        };
      });
    } else if (value) {
      this.setState(prevState => {
        return {
          contacts: contactsData.filter(contact => {
            return (
              contact.Firstname.toLowerCase().includes(value) ||
              contact.Surname.toLowerCase().includes(value)
            );
          })
        };
      });
    } else {
      this.setState({
        contacts: contactsData
      });
    }
  };

  render() {
    const { lat, lng, contacts } = this.state;
    const { classes } = this.props;
    const defaultMapProps = {
      center: { lat, lng },
      zoom: 12
    };

    return (
      <div className={classes.root}>
        <AppBar position="absolute" elevation={1} color="default">
          <Toolbar>
            <Grid
              container
              spacing={16}
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item>
                <img
                  alt="Aruba EventiVite"
                  width={120}
                  src="https://capenetworks.com/static/images/aruba/small/hpe_aruba_xs_pos_rgb.png"
                />
              </Grid>

              <Grid item>
                <div className={classes.search}>
                  <div className={classes.searchIcon}>
                    <SearchIcon />
                  </div>
                  <InputBase
                    placeholder="Search..."
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput
                    }}
                    onChange={this.searchHandler}
                  />
                </div>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>

        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyD4_sgtAekkhXrSTjp-TslFP-F0MWn7hac" }}
          defaultCenter={defaultMapProps.center}
          defaultZoom={defaultMapProps.zoom}
          key="map"
        >
          {contacts
            ? contacts.map((contact, index) => (
                <Marker
                  key={`${contact.Firstname}-${contact.Surname}-${index}`}
                  lat={contact.Lat}
                  lng={contact.Long}
                  contact={contact}
                />
              ))
            : null}
        </GoogleMapReact>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);
